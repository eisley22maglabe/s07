/*Conditional Statements
    - a conditional statement is one of the key features of a programming language
    Ex.
        is the container full or not?
        is the temperature higher than or equal to 40 degrees celcius?
        does the title contain an ampersand(&) character?
There are three types of conditional statements:
    - if-else statements 
    - switch statements
    - try-catch-finally statements
Operators
    - allows programming languages to execute operations or evaluations
    Assignment Operators
        - assign a value to a variable
/
// Basic Assignment Operator (=)
    // it allows us to assign a value to a variable
*/
    let variable = "initial value";

// Mathematical Operators (+, -,, /, %)
    // Whenever you have used a mathematical operator, a value is returned, it is only up to us if we save that returned value. 
    // Addition, subtraction, multiplication, division assignment operators allows us to assign the result of the operation to the value of the left operand. It allows us to save the result of a mathematical operation to the left operand.

    /*
        Addition Assignment Operator(+=):
         Left Operand
             - is the variable or value of the left side of the opeartor
         Right Operand
             - is the variable or value of the right side of the operator
         ex. sum = num 1 + num 4 > re-assigned the value of sum with the result of num1 + num4
    */

    let num1 = 5;
    let num2 = 10;
    let num3 = 4;
    let num4 = 40;

    let sum = num1 + num4;

    // num1 = num1 + num4;
    num1 += num4;
    console.log(num1);

    num2 += num3;
    console.log(num2);

    num1 += 55;
    console.log(num1);
    console.log(num4);

    let string1 = "Boston ";
    let string2 = "Celtics";
    string1 += string2;
    console.log(string1);
    console.log(string2);

    //15 += num1; it produces error
    //console.log(15);

    // subtraction assignment operator (-=)
    num1 -= num2;
    console.log("Result of subtraction assignment operator:" + num1);

    //multiplication assingment operator (*=)
    num2 *= num3;
    console.log("Result of multiplication assignment operator:" + num2);

    //division multiplication assignment operator (/=)

    num4 /= num3
    console.log("Result of divsion assignment operator:" + num4);

    //[SECTION] Arithmetic Operators
    let x = 1397;
    let y = 7831;

    let sum1 = x + y;
    console.log("Result of addition operator:" + sum1);

    let difference = y - x;
    console.log("Result of subtraction operator:" + difference);

    let product = x * y;
    console.log("Result of multiplication operator:" + product);

    let quotient = y / x;
    console.log("Result of divsion operator:" + quotient);

    let remainder = y % x;
    console.log("Result of modulus operator:" + remainder);

    //Multiple Operators and Parenthesis
    /*


        - when multiple operator are applied in a single statements,
        follosws the PEMDAS


        
    */

    let mdas = 1 + 2 - 3 * 4 / 5;
    console.log("Result of Mdas:" + mdas);

    //order of operation can be change by adding parentheses
    let pemdas = 1 + (2 - 3) * (4 / 5);
    console.log("Result of PEMDAS:" + pemdas);

    let pemdas1 = (1 + (2 - 3) ) * (4 / 5);
    console.log("Result of PEMDAS:" + pemdas1);

    // Increment and Decrement
        // increment and decrement is adding or subtracting 1 from the variable and re-assigning the new value to the variable where the incerement ore decrement was used

        let z = 1;

        // pre-fix incrementation
        ++z;
        console.log("Pre-fix increment:" + z);

        // post - fix incrementation

        z++;
        console.log("post-fix increments:" + z);

        console.log (z++);
        console.log(z);

        // Pre fix vs Post 

        console.log (z);
        console.log(z++);
        console.log(z);
        console.log(++z);

        // Pre-fix and post-fix Decrementation
        console.log (--z);
        console.log (z--);
        console.log (z);

// comparison operators

console.log (1 == 1);

let isSame = 55 == 55;
console.log(isSame);

console.log (1 == '1');
console.log(0 == false);
console.log(1 == true);
console.log("false"== false);
console.log(true == "true");

/*
    with loose comparision operator (==), values are compared and types if operands do not have the same types, it will be forced coerced/ type coerce before comparision

    if either the operand is a number of boolean, the operands are converted into numbers
        */

    console.log(true == "1");

//Strict Equality
    console.log(true === "1");

    console.log ("BTS" === "BTS");

    console.log ( "marie" === "marie");


//Inequality Operators 

console.log('1' != 1);
console.log("james" != "John");
console.log(1 != true);

// strict inequality !==

console.log("5" !== 5);
console.log(5 !== 5);

let name1 = "Jin";
let name2 = "Lebron";
let name3 = "Jungcool";
let name4 = "v"

let number1 = 50;
let number2 = 60;
let numstring1 = "50";
let numstring2 = "60";

console.log (numstring1 == number1); //true
console.log (numstring1 == number1); //false
console.log (numstring1 == number1); //false
console.log (name4 !== "num3"); //true
console.log (name4 !== "Jungcool"); //true
console.log (name4 !== "Jin"); //true

/* Relational Comparison Operators
     a comparison operator compares its operand and returns a boolean value based on whether the comparison is true
*/

let a = 500;
let b = 700;
let c = 8000;
let numString3 = "5500";

    // Greater than (>)
    console.log(a > b);
    console.log(c > y);

    // Less than (<)
    console.log(c < a); // false
    console.log(b < b); // false
    console.log(a < 1000); // true
    console.log(numString3 < 1000); // false - forced coercion to change the string into number
    console.log(numString3 < 6000); // true
    console.log(numString3 < "Jose"); // true - "5500" < Jose - that is erratic (unpredictable)

    // Greater than or Equal to (>=)
    console.log(c >= 10000); // false
    //console.log(b >= x); // false
    console.log(b >= a); // true

    // Less than or Equal to (<=)
    console.log(a <= b); //true
    console.log(c <= a); //false

/* Logical Operators
    AND operator (&&)
        - both operands on the left and right or all operands must be true otherwise false

        T && T = true
        T && F = false
        F && T = false
        F && F = false
*/

    let isAdmin = false;
    let isRegistered = true;
    let isLegalAge = true;

    let authorization1 = isAdmin && isRegistered;
    console.log(authorization1); //false

    let authorization2 = isLegalAge && isRegistered;
    console.log(authorization2); //true

    let requiredLevel = 95;
    let requiredAge = 18;

    let authorization3 = isRegistered && requiredLevel === 25;
    console.log(authorization3); //false

    let authorization4 = isRegistered && isLegalAge && requiredLevel === 95;
    console.log(authorization4);

    let userName1 = "gamer2001";
    let userName2 = "shadow1991"; 
    let userAge = 15;
    let userAge2 = 30;

    let registration1 = userName1.length > 8 && userAge >= requiredAge;
    // .length is a property of string which determines the number of characters in the string
    console.log(registration1); // false - userAge does not meet the required age = 18

    let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
    console.log(registration2); // true

    /* OR Operator (|| - double pipe)
        returns true if at least one of the operands are true.
        T || T = true
        T || F = true
        F || T = true
        F || F = false
    */
    let userLevel = 100;
    let userLevel2 = 65;

    let guildRequirment1 = isRegistered && userLevel >= requiredLevel && userAge >= requiredAge;
    console.log(guildRequirment1); // false

    let guildRequirment2 = isRegistered || userLevel >= requiredLevel || userAge >= requiredAge;
    console.log(guildRequirment2); // true

    let guildRequirment3 = userLevel >= requiredLevel ||  userAge >= requiredAge;
    console.log(guildRequirment3); //true

    let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
    console.log(guildAdmin); //false

    // Not Operator - it turns a boolean into the opposite value

    let guildAdmin2 = !isAdmin || userLevel2 >= requiredLevel;
    console.log(guildAdmin2); // true

    console.log(!isRegistered); // false

    // Conditional
        // if-else statements - will run a block of code if the condition specified is true or results to true

        let userName3 = "Crusader_1993";
        let userLevel3 = 25;
        let userAge3 = 20;

        // if(true){
        //  alert("We just run an if condition!")
        // };

        if(userName3.length > 10){
            console.log("Welcome to Game Online!");
        };

        if(userLevel3 >= requiredLevel){
            console.log("You are qualified to join the guild.");
        };

        // else statement will run if the condition given is false or results to false

        if(userName3.length > 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
            console.log("Thank you for joining the Noobies Guild.");
        } else {
            console.log("You are too strong to be a n00b. :");
        };

        if(userName3.length >= 10 && userLevel3 <=25 && userAge >= requiredAge){
            console.log("Thank you for joining the Noobies Guild");
        } else if (userName3 > 25) {
            console.log("You are too strong to be a noob.");
        } else if (userName3.length < 10){
            console.log("Username is too short")
        };

        // if-else in function
        function addNum(num1,num2){
            if(typeof num1 === 'number1' && typeof num2 === 'number1'){
                console.log("run only if both arguements passed are number types.");
                console.log (num1 + num2);

            } else {
                console.log("one or both of hte arguements are not numbers");
            };
        };

        addNum(5,"2");


//mini-activity
function login(userName1,userName2){
           if(typeof userName1 ==='string'&& typeof userName2 === 'string'){
            console.log("Both arguments are strings");
        }
        if(userName1.length>=8 && userName2.length>=8)
           {
                console.log("logging in")
           }
            else if (userName2.length <8)
           {
                console.log(alert("password is too short"))
           }
            else if (userName1.length<8)
           {
                console.log(alert("username is too short"))
           }
            else{
                console.log(alert("Credentials are too short"));
           }
    }
    login('sdafadfa2','2a32323323');

function shirtColor(day){
    let days = day.toLowerCase();
    let colorShirt = "";

    if(typeof day === 'string'){
            console.log("The argument is a string")
        }

    else{console.log(alert("Please input a proper string"));}

    if(day = "monday"){
        colorShirt = "Black"
        console.log(alert("Today is " + day + " Wear " + colorShirt))
    }

    else if(day = "tuesday"){
        colorShirt = "Green"
        console.log(alert("Today is " + day + " Wear " + colorShirt))
    }

    else if(day = "wednesday"){
        colorShirt = "Yellow"
        console.log(alert("Today is " + day + " Wear " + colorShirt))
    }

    else if(day = "thursday"){
        colorShirt = "Red"
        console.log(alert("Today is " + day + " Wear " + colorShirt))
    }

    else if(day = "friday"){
        colorShirt = "Violet"
        console.log(alert("Today is " + day + " Wear " + colorShirt))
    }

    else if(day = "saturday"){
        colorShirt = "Blue"
        console.log(alert("Today is " + day + " Wear " + colorShirt))
    }

    else if(day = "sunday"){
        colorShirt = "White"
        console.log(alert("Today is " + day + " Wear " + colorShirt))
    }

    else{console.log(alert("Invalid"));
    }
}

shirtColor("Sunday");